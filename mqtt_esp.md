# Async MQTT - Тестирование

```c++
#include <Arduino.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <Ticker.h>
#include <AsyncMqttClient.h>
#include <ArduinoJson.h>
#include <EncButton.h>

#define WIFI_SSID "Guest_bli3"
#define WIFI_PASSWORD "987987987"
#define MQTT_HOST "broker.emqx.io"
#define MQTT_PORT 1883

#define MQTT_DEVICE_ADR "bli_3/main" // изменть имя топика

#define MQTT_STATE_TOPIC MQTT_DEVICE_ADR "/state"
#define MQTT_CONTROL_TOPIC MQTT_DEVICE_ADR "/control"
#define MQTT_ONLINE_TOPIC MQTT_DEVICE_ADR "/online"

AsyncMqttClient mqttClient;
Ticker mqttReconnectTimer;

WiFiEventHandler wifiConnectHandler;
WiFiEventHandler wifiDisconnectHandler;
Ticker wifiReconnectTimer;

void connectToWifi()
{
  Serial.println("Connecting to Wi-Fi...");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
}

void connectToMqtt()
{
  mqttClient.connect();
}

void onWifiConnect(const WiFiEventStationModeGotIP &event)
{
  connectToMqtt();
}

void onWifiDisconnect(const WiFiEventStationModeDisconnected &event)
{
  mqttReconnectTimer.detach(); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
  wifiReconnectTimer.once(2, connectToWifi);
}

void subscribe_topics()
{
  mqttClient.subscribe(MQTT_CONTROL_TOPIC, 2);
}

void SyncStates()
{
}

void onMqttConnect(bool sessionPresent)
{

  Serial.println("MQTT client connected");

  mqttClient.publish(MQTT_ONLINE_TOPIC, 0, true, "1");

  SyncStates();
  subscribe_topics();
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason)
{
  if (WiFi.isConnected())
  {
    mqttReconnectTimer.once(2, connectToMqtt);
  }
}

void onMqttMessage(char *topic, char *payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total)
{
  Serial.println(topic);

  StaticJsonDocument<300> doc;                                // Объект JSON
  DeserializationError error = deserializeJson(doc, payload); // Парсинг из строки  в структуру

  // Не получилось распарсить данные
  if (error)
  {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    return;
  }
  else
  {
    // Извлекаем ключ "test"
    const char *myStr = doc["test"];
    Serial.println(myStr);

    doc["my_key"] = "Hello";

    char buffer[256];
    size_t n = serializeJson(doc, buffer);
    mqttClient.publish(MQTT_STATE_TOPIC,2, true, buffer, n);
  }
}

void setup()
{

  Serial.begin(9600);

  wifiConnectHandler = WiFi.onStationModeGotIP(onWifiConnect);
  wifiDisconnectHandler = WiFi.onStationModeDisconnected(onWifiDisconnect);

  mqttClient.onConnect(onMqttConnect);
  mqttClient.onDisconnect(onMqttDisconnect);
  mqttClient.onMessage(onMqttMessage);
  mqttClient.setServer(MQTT_HOST, MQTT_PORT);
  mqttClient.setWill(MQTT_ONLINE_TOPIC, 0, true, "0");
  connectToWifi();
}

void loop()
{
}
```


# Отправка данных на mqtt
``` c++
#include <Arduino.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <Ticker.h>
#include <AsyncMqttClient.h>
#include <ArduinoJson.h>
#include "DHT.h"
#include "GyverTimer.h"

#include <EncButton.h>

#define WIFI_SSID "Guest_bli3"
#define WIFI_PASSWORD "987987987"
#define MQTT_HOST "broker.emqx.io"
#define MQTT_PORT 1883

#define MQTT_DEVICE_ADR "bli_3/main" // изменть имя топика

#define MQTT_STATE_TOPIC MQTT_DEVICE_ADR "/state" // соедедение строк MQTT_DEVICE_ADR + "/state"
#define MQTT_CONTROL_TOPIC MQTT_DEVICE_ADR "/control"
#define MQTT_ONLINE_TOPIC MQTT_DEVICE_ADR "/online"

// Итоговый топик отпправки: bli_3/main/control
// Итоговый топик получения: bli_3/main/state

#define DHTPIN 14 // Пин датчика
// Одна из следующих строк закоментирована. Снимите комментарий, если подключаете датчик DHT11 к arduino
DHT dht(DHTPIN, DHT22); // Инициация датчика
// DHT dht(DHTPIN, DHT22); - для DHT22

GTimer dht_timer(MS); // для опроса датчика

AsyncMqttClient mqttClient;
Ticker mqttReconnectTimer;

WiFiEventHandler wifiConnectHandler;
WiFiEventHandler wifiDisconnectHandler;
Ticker wifiReconnectTimer;

void connectToWifi()
{
  Serial.println("Connecting to Wi-Fi...");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
}

void connectToMqtt()
{
  mqttClient.connect();
}

void onWifiConnect(const WiFiEventStationModeGotIP &event)
{
  connectToMqtt();
}

void onWifiDisconnect(const WiFiEventStationModeDisconnected &event)
{
  mqttReconnectTimer.detach(); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
  wifiReconnectTimer.once(2, connectToWifi);
}

void subscribe_topics()
{
  mqttClient.subscribe(MQTT_CONTROL_TOPIC, 2);
}

void SyncStates()
{
}

void onMqttConnect(bool sessionPresent)
{

  Serial.println("MQTT client connected");

  mqttClient.publish(MQTT_ONLINE_TOPIC, 0, true, "1");

  SyncStates();
  subscribe_topics();
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason)
{
  if (WiFi.isConnected())
  {
    mqttReconnectTimer.once(2, connectToMqtt);
  }
}

void onMqttMessage(char *topic, char *payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total)
{

  Serial.println(topic);

  StaticJsonDocument<300> doc;                                // Объект JSON
  DeserializationError error = deserializeJson(doc, payload); // Парсинг из строки  в структуру

  // Не получилось распарсить данные
  if (error)
  {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    return;
  }
  else
  {
    // Извлекаем ключ "test"
    const char *myStr = doc["test"];
    Serial.println(myStr);

    doc["my_key"] = "Hello";

    char buffer[256];
    size_t n = serializeJson(doc, buffer);
    mqttClient.publish(MQTT_STATE_TOPIC, 2, true, buffer, n);
  }
}

void setup()
{

  Serial.begin(9600);
  dht.begin();
  dht_timer.setInterval(2000); // интервал опроса датчика

  wifiConnectHandler = WiFi.onStationModeGotIP(onWifiConnect);
  wifiDisconnectHandler = WiFi.onStationModeDisconnected(onWifiDisconnect);

  mqttClient.onConnect(onMqttConnect);
  mqttClient.onDisconnect(onMqttDisconnect);
  mqttClient.onMessage(onMqttMessage);
  mqttClient.setServer(MQTT_HOST, MQTT_PORT);
  mqttClient.setWill(MQTT_ONLINE_TOPIC, 0, true, "0");
  connectToWifi();
}

void loop()
{
  if (dht_timer.isReady())
  {
    static float last_t, last_h;

    float h = dht.readHumidity();    // Измеряем влажность
    float t = dht.readTemperature(); // Измеряем температуру

    StaticJsonDocument<300> doc;

    if (isnan(h) || isnan(t))
    { // Проверка. Если не удается считать показания, выводится «Ошибка считывания», и программа завершает работу
      Serial.println("Ошибка считывания");
    }
    else
    {

      doc["temp"] = t;
      doc["humd"] = h;

      char buffer[256];
      size_t n = serializeJson(doc, buffer);
      
      mqttClient.publish(MQTT_STATE_TOPIC, 2, true, buffer, n);
    }
  }
}
```



# Пример 1 дня с mqtt

```c++
#include <Arduino.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <Ticker.h>
#include <AsyncMqttClient.h>
#include <ArduinoJson.h>
#include "DHT.h"
#include "GyverTimer.h"

#include <EncButton.h>

#define WIFI_SSID "Guest_bli3"
#define WIFI_PASSWORD "987987987"
#define MQTT_HOST "broker.emqx.io"
#define MQTT_PORT 1883

#define MQTT_DEVICE_ADR "bli_3/your_name" // изменть имя топика

#define MQTT_STATE_TOPIC MQTT_DEVICE_ADR "/state" // соедедение строк MQTT_DEVICE_ADR + "/state"
#define MQTT_CONTROL_TOPIC MQTT_DEVICE_ADR "/control"
#define MQTT_ONLINE_TOPIC MQTT_DEVICE_ADR "/online"

// Итоговый топик отпправки: bli_3/main/control
// Итоговый топик получения: bli_3/main/state

#define DHTPIN 14 // Пин датчика
// Одна из следующих строк закоментирована. Снимите комментарий, если подключаете датчик DHT11 к arduino
DHT dht(DHTPIN, DHT22); // Инициация датчика
// DHT dht(DHTPIN, DHT22); - для DHT22

#define LED_PIN_BLINK 12
#define LED_PIN_ERROR 12

// MS - время в милисекундах !!!
// US - время в микросекундах  !!!

GTimer dht_timer(MS); // для опроса датчика
GTimer led_timer(MS); // для мигания свет-да

AsyncMqttClient mqttClient;
Ticker mqttReconnectTimer;

WiFiEventHandler wifiConnectHandler;
WiFiEventHandler wifiDisconnectHandler;
Ticker wifiReconnectTimer;

void connectToWifi()
{
  Serial.println("Connecting to Wi-Fi...");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
}

void connectToMqtt()
{
  mqttClient.connect();
}

void onWifiConnect(const WiFiEventStationModeGotIP &event)
{
  connectToMqtt();
}

void onWifiDisconnect(const WiFiEventStationModeDisconnected &event)
{
  mqttReconnectTimer.detach(); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
  wifiReconnectTimer.once(2, connectToWifi);
}

void subscribe_topics()
{
  mqttClient.subscribe(MQTT_CONTROL_TOPIC, 2);
}

void SyncStates()
{
}

void onMqttConnect(bool sessionPresent)
{

  Serial.println("MQTT client connected");

  mqttClient.publish(MQTT_ONLINE_TOPIC, 0, true, "1");

  SyncStates();
  subscribe_topics();
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason)
{
  if (WiFi.isConnected())
  {
    mqttReconnectTimer.once(2, connectToMqtt);
  }
}

void onMqttMessage(char *topic, char *payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total)
{

  Serial.println(topic);

  StaticJsonDocument<300> doc;                                // Объект JSON
  DeserializationError error = deserializeJson(doc, payload); // Парсинг из строки  в структуру

  // Не получилось распарсить данные
  if (error)
  {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    return;
  }
  else
  {
    // Извлекаем ключ "test"
    const char *myStr = doc["test"];
    Serial.println(myStr);

    doc["my_key"] = "Hello";

    char buffer[256];
    size_t n = serializeJson(doc, buffer);
    mqttClient.publish(MQTT_STATE_TOPIC, 2, true, buffer, n);
  }
}

void setup()
{

  Serial.begin(9600);
  dht.begin();
  
  pinMode(LED_PIN_BLINK, OUTPUT);
  pinMode(LED_PIN_ERROR, OUTPUT);

  dht_timer.setInterval(2000); // интервал опроса датчика

  wifiConnectHandler = WiFi.onStationModeGotIP(onWifiConnect);
  wifiDisconnectHandler = WiFi.onStationModeDisconnected(onWifiDisconnect);

  mqttClient.onConnect(onMqttConnect);
  mqttClient.onDisconnect(onMqttDisconnect);
  mqttClient.onMessage(onMqttMessage);
  mqttClient.setServer(MQTT_HOST, MQTT_PORT);
  mqttClient.setWill(MQTT_ONLINE_TOPIC, 0, true, "0");
  connectToWifi();
}

void loop()
{
  static int dht_state = 1;

  if (dht_timer.isReady())
  {

    static float last_t, last_h;

    float h = dht.readHumidity();    // Измеряем влажность
    float t = dht.readTemperature(); // Измеряем температуру

    if (isnan(h) || isnan(t))
    { // Проверка. Если не удается считать показания, выводится «Ошибка считывания», и программа завершает работу
      Serial.println("Ошибка считывания");

      if (dht_state)
      {
        digitalWrite(LED_PIN_BLINK, LOW);
        led_timer.setInterval(1000);
        dht_state = 0;
      }
    }
    else
    {
      if (!dht_state)
      {
        digitalWrite(LED_PIN_ERROR, LOW);
        dht_state = 1;
      }

      if (fabs(t - last_t) >= 1 || fabs(h - last_h) >= 1)
      {

        last_h = h;
        last_t = t; // Присваивание тек. значения

        led_timer.setMode(TIMER_TIMEOUT); // режим таймер с запуском
        led_timer.setTimeout(200);        // после изменения темп/влаж гореть 200 мс.

        led_timer.start();
        digitalWrite(LED_PIN_BLINK, 1);

        Serial.print("Влажность: ");
        Serial.print(h);
        Serial.print(" %\t");
        Serial.print("Температура: ");
        Serial.print(t);
        Serial.println(" *C "); // Вывод показателей на экран

        static StaticJsonDocument<300> doc;
        doc.clear();

        doc["temp"] = t;
        doc["humd"] = h;

        char buffer[256];
        size_t n = serializeJson(doc, buffer);

        mqttClient.publish(MQTT_STATE_TOPIC, 2, true, buffer, n);
      }
    }
  }

  if (led_timer.isReady())
  {
    if (dht_state)
      digitalWrite(LED_PIN_BLINK, LOW);
    else
      digitalWrite(LED_PIN_ERROR, !digitalRead(LED_PIN_ERROR));
  }
}
```

# Бот с меню от Динияра

```c++
import asyncio
import os
import signal
import time

import logging
import typing
import uuid
import json

from aiogram import Bot, Dispatcher, executor, types

API_TOKEN = '5615724495:AAFS8PhhRtjEfrmhSKaS2P_GlQs2gxPr15g'
USER_CHAT_ID = "1860075676"

logging.basicConfig(level=logging.INFO)
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)

from gmqtt import Client as MQTTClient

client = MQTTClient(f"bli_3/{uuid.uuid4().hex[:6]}/state")

STOP = asyncio.Event()


def on_connect(client, flags, rc, properties):
    print('Connected')


def on_message(client, topic, payload, qos, properties):
    global dic
    dic = json.loads(payload)

@dp.message_handler(commands="start")
async def start(message: types.Message):
    await message.answer("Привет")

    keyboard = types.ReplyKeyboardMarkup()
    temp = types.KeyboardButton(text="Отправить температуру")
    humd = types.KeyboardButton(text="Отправить влажность")
    keyboard.add(temp, humd)
    await message.answer("Что тебя интересует?", reply_markup=keyboard)

@dp.message_handler()
async def answer(message: types.Message):
    if message.text == 'Отправить температуру':
        await bot.send_message(USER_CHAT_ID,
                                   f"Температура: {dic['temp']}\n")

    elif message.text == 'Отправить влажность':
        await bot.send_message(USER_CHAT_ID,
                                   f"Влажность: {dic['humd']}")


def on_disconnect(client, packet, exc=None):
    print('Disconnected')


def on_subscribe(client, mid, qos, properties):
    print('SUBSCRIBED')


def ask_exit(*args):
    STOP.set()


async def bot_start():
    await bot.delete_webhook(drop_pending_updates=True)
    await dp.start_polling()


async def main():
    # имя подключения

    client.on_connect = on_connect
    client.on_message = on_message
    client.on_disconnect = on_disconnect
    client.on_subscribe = on_subscribe

    await client.connect('broker.emqx.io')

    asyncio.create_task(bot_start())

    # здесь подписываемся на темы
    client.subscribe("bli_3/my/state")

    await STOP.wait()
    await client.disconnect()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
```

# Итог дня, упрвление реле через mqtt

```json
// запрос на топик xxxx/control
{
  "command" : "on" // on / off
}

// ответ при включении реле
// xxxx/state
{
  "relay_state" : 1 // 1 / 0
}

```


```c++
#include <Arduino.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <Ticker.h>
#include <AsyncMqttClient.h>
#include <ArduinoJson.h>
#include "DHT.h"
#include "GyverTimer.h"
#include <Servo.h>

#include <EncButton.h>

#define WIFI_SSID "Guest_bli3"
#define WIFI_PASSWORD "987987987"
#define MQTT_HOST "broker.emqx.io"
#define MQTT_PORT 1883

#define MQTT_DEVICE_ADR "bli_3/test_1" // изменть имя топика

#define MQTT_STATE_TOPIC MQTT_DEVICE_ADR "/state" // соедедение строк MQTT_DEVICE_ADR + "/state"
#define MQTT_CONTROL_TOPIC MQTT_DEVICE_ADR "/control"
#define MQTT_ONLINE_TOPIC MQTT_DEVICE_ADR "/online"

// Итоговый топик отпправки: bli_3/main/control
// Итоговый топик получения: bli_3/main/state

#define DHTPIN 14 // Пин датчика
// Одна из следующих строк закоментирована. Снимите комментарий, если подключаете датчик DHT11 к arduino
DHT dht(DHTPIN, DHT22); // Инициация датчика
// DHT dht(DHTPIN, DHT22); - для DHT22

#define LED_PIN_BLINK 12
#define LED_PIN_ERROR 12

#define RELAY_PIN 13

// MS - время в милисекундах !!!
// US - время в микросекундах  !!!

GTimer dht_timer(MS); // для опроса датчика
GTimer led_timer(MS); // для мигания свет-да

Servo myServo();

AsyncMqttClient mqttClient;
Ticker mqttReconnectTimer;

WiFiEventHandler wifiConnectHandler;
WiFiEventHandler wifiDisconnectHandler;
Ticker wifiReconnectTimer;

void connectToWifi()
{
  Serial.println("Connecting to Wi-Fi...");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
}

void connectToMqtt()
{
  mqttClient.connect();
}

void onWifiConnect(const WiFiEventStationModeGotIP &event)
{
  connectToMqtt();
}

void onWifiDisconnect(const WiFiEventStationModeDisconnected &event)
{
  mqttReconnectTimer.detach(); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
  wifiReconnectTimer.once(2, connectToWifi);
}

void subscribe_topics()
{
  mqttClient.subscribe(MQTT_CONTROL_TOPIC, 2);
}

void SyncStates()
{
}

void onMqttConnect(bool sessionPresent)
{

  Serial.println("MQTT client connected");

  mqttClient.publish(MQTT_ONLINE_TOPIC, 0, true, "1");

  SyncStates();
  subscribe_topics();
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason)
{
  if (WiFi.isConnected())
  {
    mqttReconnectTimer.once(2, connectToMqtt);
  }
}

void onMqttMessage(char *topic, char *payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total)
{

  Serial.println(topic);

  StaticJsonDocument<300> doc;                                // Объект JSON
  DeserializationError error = deserializeJson(doc, payload); // Парсинг из строки  в структуру

  // Не получилось распарсить данные
  if (error)
  {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    return;
  }
  else
  {
    // Проверяем есть ли ключ command
    if (doc.containsKey("command"))
    {
      if (strcmp(doc["command"], "on") == 0)
        digitalWrite(RELAY_PIN, 1);
      else
        digitalWrite(RELAY_PIN, 0);
    }

    // Очищаем документ, при этом используем уже созданый объект
    doc.clear();

    // Записываем ключ о состоянии реле
    doc["relay_state"] = digitalRead(RELAY_PIN);

    // Отправляем данные на сервер
    char buffer[256];
    size_t n = serializeJson(doc, buffer);
    mqttClient.publish(MQTT_STATE_TOPIC, 2, true, buffer, n);
  }
}

void setup()
{

  Serial.begin(9600);
  dht.begin();

  pinMode(LED_PIN_BLINK, OUTPUT);
  pinMode(LED_PIN_ERROR, OUTPUT);
  pinMode(RELAY_PIN, OUTPUT);

  dht_timer.setInterval(2000); // интервал опроса датчика

  wifiConnectHandler = WiFi.onStationModeGotIP(onWifiConnect);
  wifiDisconnectHandler = WiFi.onStationModeDisconnected(onWifiDisconnect);

  mqttClient.onConnect(onMqttConnect);
  mqttClient.onDisconnect(onMqttDisconnect);
  mqttClient.onMessage(onMqttMessage);
  mqttClient.setServer(MQTT_HOST, MQTT_PORT);
  mqttClient.setWill(MQTT_ONLINE_TOPIC, 0, true, "0");
  connectToWifi();
}

void loop()
{
  static int dht_state = 1;

  if (dht_timer.isReady())
  {

    static float last_t, last_h;

    float h = dht.readHumidity();    // Измеряем влажность
    float t = dht.readTemperature(); // Измеряем температуру

    StaticJsonDocument<300> doc;

    if (isnan(h) || isnan(t))
    { // Проверка. Если не удается считать показания, выводится «Ошибка считывания», и программа завершает работу
      Serial.println("Ошибка считывания");

      if (dht_state)
      {
        doc["error"] = true;
        digitalWrite(LED_PIN_BLINK, LOW);
        led_timer.setInterval(1000);
        dht_state = 0;
      }
    }
    else
    {
      if (!dht_state)
      {
        digitalWrite(LED_PIN_ERROR, LOW);
        dht_state = 1;
      }

      if (fabs(t - last_t) >= 1 || fabs(h - last_h) >= 1)
      {

        last_h = h;
        last_t = t; // Присваивание тек. значения

        led_timer.setMode(TIMER_TIMEOUT); // режим таймер с запуском
        led_timer.setTimeout(200);        // после изменения темп/влаж гореть 200 мс.

        led_timer.start();
        digitalWrite(LED_PIN_BLINK, 1);

        Serial.print("Влажность: ");
        Serial.print(h);
        Serial.print(" %\t");
        Serial.print("Температура: ");
        Serial.print(t);
        Serial.println(" *C "); // Вывод показателей на экран

        doc["temp"] = t;
        doc["humd"] = h;

        char buffer[256];
        size_t n = serializeJson(doc, buffer);

        mqttClient.publish(MQTT_STATE_TOPIC, 2, true, buffer, n);
      }
    }
  }

  if (led_timer.isReady())
  {
    if (dht_state)
      digitalWrite(LED_PIN_BLINK, LOW);
    else
      digitalWrite(LED_PIN_ERROR, !digitalRead(LED_PIN_ERROR));
  }
}
```