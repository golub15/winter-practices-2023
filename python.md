Файл по  python


https://docs.python.org/3/library/asyncio-task.html#coroutine
Асинхронность.

https://docs.aiogram.dev/en/latest/examples/echo_bot.html - примеры ботов


# Таймер

```python
import asyncio


async def my_func_1():
    while 1:
        print("123")
        await asyncio.sleep(1)


async def my_func_2():
    while 1:
        print("456")
        await asyncio.sleep(0.5)

loop = asyncio.get_event_loop()
loop.create_task(my_func_1())
loop.create_task(my_func_2())
loop.run_forever()
```

```python
import asyncio
import time


async def my_func_1(need_time, prec):
    while 1:
        await asyncio.sleep(prec)
        need_time -= prec
        if need_time > 0:
            print(f"Осталось: {need_time}")
        else:
            print(f"Время истекло")
            break


# async def my_func_2():
#     while 1:
#         print(f"2_task>{time.strftime('%X')}")
#         await asyncio.sleep(5)

need_time = int(input("Введите время в сек: "))
loop = asyncio.get_event_loop()
loop.create_task(my_func_1(need_time, 1))
loop.create_task(my_func_1(need_time, 2.5))
loop.run_forever()
```

# эхо Бот

https://docs.aiogram.dev/en/latest/examples/echo_bot.html
```python
import logging

from aiogram import Bot, Dispatcher, executor, types

API_TOKEN = '++++'

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)


@dp.message_handler(commands=['start', 'help'])
async def send_welcome(message: types.Message):
    """
    This handler will be called when user sends `/start` or `/help` command
    """
    await message.reply("Hi!\nI'm EchoBot!\nPowered by aiogram.")


@dp.message_handler()
async def echo(message: types.Message):
    # old style:
    # await bot.send_message(message.chat.id, message.text)

    await message.answer(message.text)


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
```

# Таймер, запускается при новом сообщении

```python
import asyncio
import logging

from aiogram import Bot, Dispatcher, executor, types

API_TOKEN = '++++'

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)


@dp.message_handler(commands=['start', 'help'])
async def send_welcome(message: types.Message):
    """
    This handler will be called when user sends `/start` or `/help` command
    """
    await message.reply("Hi!\nI'm EchoBot!\nPowered by aiogram.")


@dp.message_handler()
async def echo(message: types.Message):
    # old style:
    # await bot.send_message(message.chat.id, message.text)
    if not  message.text.isdigit():
        await message.answer(f"Не числа")
        return
    need_time = int(message.text)
    prec = 0.5
    while 1:
        await asyncio.sleep(prec)
        need_time -= prec
        if need_time > 0:
            await message.answer(f"Осталось: {need_time}")
        else:
            await message.answer(f"Время истекло")
            break



if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)

```

# Таймер с функцией стоп

```python
import asyncio
import logging
import typing

from aiogram import Bot, Dispatcher, executor, types

API_TOKEN = '++++'

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)


@dp.message_handler(commands=['start', 'help'])
async def send_welcome(message: types.Message):
    """
    This handler will be called when user sends `/start` or `/help` command
    """
    await message.reply("Hi!\nI'm EchoBot!\nPowered by aiogram.")


async def timer(chat_id, text):
    try:
        if not text.isdigit():
            await bot.send_message(chat_id, f"Не числа")
            return
        need_time = int(text)
        prec = 0.5
        while 1:
            await asyncio.sleep(prec)
            need_time -= prec
            if need_time > 0:
                await bot.send_message(chat_id, f"Осталось: {need_time}")
            else:
                await bot.send_message(chat_id, f"Время истекло")
                break
    except asyncio.CancelledError:
        await bot.send_message(chat_id, f"таймер остановлен")


task: asyncio.Task = None

@dp.message_handler()
async def echo(message: types.Message):
    global task
    if task is None or task.done():

        task = asyncio.create_task(timer(message.chat.id, message.text))

    else:
        if message.text == "stop" and task is not None:
            task.cancel()

    # asyncio.all_tasks()
    # timer_tasks[message.chat.id] =


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
```

# MQTT  клиент

``` python
import asyncio
import os
import signal
import time

from gmqtt import Client as MQTTClient

STOP = asyncio.Event()


def on_connect(client, flags, rc, properties):
    print('Connected')
    client.subscribe('TEST/#', qos=0)


def on_message(client, topic, payload, qos, properties):
    print('RECV MSG:', payload.decode("utf-8"))


def on_disconnect(client, packet, exc=None):
    print('Disconnected')

def on_subscribe(client, mid, qos, properties):
    print('SUBSCRIBED')

def ask_exit(*args):
    STOP.set()

async def main(broker_host, token):
    client = MQTTClient("782732378")

    client.on_connect = on_connect
    client.on_message = on_message
    client.on_disconnect = on_disconnect
    client.on_subscribe = on_subscribe

    await client.connect(broker_host)

    client.publish('TEST/TIME', str(time.time()), qos=1)
    
    #здесь подписываемся на темы
    client.subscribe("bli_3/test")

    await STOP.wait()
    await client.disconnect()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()

    host = 'broker.emqx.io'
    loop.run_until_complete(main(host, ""))
```

# MQTT ФЛУД
``` python
import asyncio
import os
import signal
import time

from gmqtt import Client as MQTTClient

client = MQTTClient("bli_3/client")

STOP = asyncio.Event()


def on_connect(client, flags, rc, properties):
    print('Connected')
    client.subscribe('TEST/#', qos=0)


def on_message(client, topic, payload, qos, properties):
    print('RECV MSG:', payload.decode("utf-8"))


def on_disconnect(client, packet, exc=None):
    print('Disconnected')


def on_subscribe(client, mid, qos, properties):
    print('SUBSCRIBED')


def ask_exit(*args):
    STOP.set()


async def send_msg():
    while 1:
        await asyncio.sleep(5)
        client.publish("bli_3/test", "haha")


async def main(broker_host, token):
    # имя подключения

    client.on_connect = on_connect
    client.on_message = on_message
    client.on_disconnect = on_disconnect
    client.on_subscribe = on_subscribe

    await client.connect(broker_host)

    asyncio.create_task(send_msg())

    # здесь подписываемся на темы
    client.subscribe("bli_3/test")

    await STOP.wait()
    await client.disconnect()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    host = 'broker.emqx.io'
    loop.run_until_complete(main(host, ""))
```

``` python 
import asyncio
import os
import signal
import time

import logging
import typing

from aiogram import Bot, Dispatcher, executor, types

API_TOKEN = '++++'

logging.basicConfig(level=logging.INFO)
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)

from gmqtt import Client as MQTTClient

client = MQTTClient("bli_3/client")

STOP = asyncio.Event()


def on_connect(client, flags, rc, properties):
    print('Connected')
    client.subscribe('TEST/#', qos=0)


def on_message(client, topic, payload, qos, properties):
    print('RECV MSG:', payload.decode("utf-8"))


def on_disconnect(client, packet, exc=None):
    print('Disconnected')


def on_subscribe(client, mid, qos, properties):
    print('SUBSCRIBED')


def ask_exit(*args):
    STOP.set()


async def send_msg():
    while 1:
        await asyncio.sleep(5)
        client.publish("bli_3/test", "ПРИВЕТ")


@dp.message_handler()
async def echo(message: types.Message):
    client.publish("bli3/topic_2", message.text)


async def bot_start():
    await bot.delete_webhook(drop_pending_updates=True)
    await dp.start_polling()


async def main():
    # имя подключения

    client.on_connect = on_connect
    client.on_message = on_message
    client.on_disconnect = on_disconnect
    client.on_subscribe = on_subscribe

    await client.connect('broker.emqx.io')

    asyncio.create_task(bot_start())
    asyncio.create_task(send_msg())

    # здесь подписываемся на темы
    #client.subscribe("bli_3/test")

    await STOP.wait()
    await client.disconnect()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
```

# Отсылкает сообщения из mqtt

``` python
import asyncio
import os
import signal
import time

import logging
import typing
import uuid

from aiogram import Bot, Dispatcher, executor, types

API_TOKEN = '++'
USER_CHAT_ID = ""


logging.basicConfig(level=logging.INFO)
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)

from gmqtt import Client as MQTTClient

client = MQTTClient(f"bli_3/{uuid.uuid4().hex[:6]}")

STOP = asyncio.Event()


def on_connect(client, flags, rc, properties):
    print('Connected')


async def send(text):
    print("text")
    await bot.send_message(USER_CHAT_ID, text)


def on_message(client, topic, payload, qos, properties):
    asyncio.create_task(send(payload.decode("utf-8")))


def on_disconnect(client, packet, exc=None):
    print('Disconnected')


def on_subscribe(client, mid, qos, properties):
    print('SUBSCRIBED')


def ask_exit(*args):
    STOP.set()


@dp.message_handler()
async def echo(message: types.Message):
    print("Чат: ",message.chat.id)
    
    client.publish("bli3/topic_2", message.text)


async def bot_start():
    await bot.delete_webhook(drop_pending_updates=True)
    await dp.start_polling()


async def main():
    # имя подключения

    client.on_connect = on_connect
    client.on_message = on_message
    client.on_disconnect = on_disconnect
    client.on_subscribe = on_subscribe

    await client.connect('broker.emqx.io')

    asyncio.create_task(bot_start())

    # здесь подписываемся на темы
    client.subscribe("bli3/topic_2")

    await STOP.wait()
    await client.disconnect()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
```