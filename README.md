# Winter practices 2023


**Реализация 1**
```c++
#define MY_PERIOD 500  // период в мс


uint32_t tmr1;         // ГЛОБАЛЬНАЯ ПЕРЕМЕННАЯ



void setup() {}
void loop() {
  if (millis() - tmr1 >= MY_PERIOD) {  // ищем разницу
    tmr1 = millis();                   // сброс таймера
    // выполнить действие
  }
}
```

**Реализация 2**
```c++
#define MY_PERIOD 500  // период в мс

void setup() {}
void loop() {

  static uint32_t tmr1;         // ГЛОБАЛЬНАЯ ПЕРЕМЕННАЯ, но уже в функции
  if (millis() - tmr1 >= MY_PERIOD) {  // ищем разницу
    tmr1 = millis();                   // сброс таймера
    // выполнить действие
  }
}
```



# День 1

## Подготовка
https://alexgyver.ru/lessons/short-reference/ - Шпарглака по языку C++ и объектам для Arduino

https://alexgyver.ru/lessons/how-to-sketch/ - как писать прошивку


1. проверка и установка Arduino IDE, или Platformio

## Материал

Знакомство с Arduino
- Плтаформа UNO



Практика №1: Управление портом к которому подключен светодиод:
![alt text](https://content.instructables.com/F60/IZJJ/I8ZQZMO9/F60IZJJI8ZQZMO9.png)

Задача - мигать светодиодом с периодом 1 секунда и длительностью горения 1 периодом

Релаизация:
```c++
#define LED_PIN 7
void setup() {
  pinMode(LED_PIN, OUTPUT);
}

//мигание светодидом часттой 1 сек.
void loop() {
  digitalWrite(LED_PIN, HIGH);   
  delay(1000); // 1 вызов                                 
  digitalWrite(LED_PIN, LOW);    
  delay(1000); // 2 вызов                                      
}
```
Для справки
```c++
#define LED_PIN 7                  //Подставляет переменную в функцию
pinMode(LED_PIN, OUTPUT);          //Установка порта на выход                           
digitalWrite(LED_PIN, LOW);        //Управление портом LED_PIN LOW - низкий уровень, HIGH - высокий уровень
                                   //Поняите "низкий уровень" это тоже самое если вы соедените вход светодиода с землей (GND)
                                   //Поняите "высокий уровень" это тоже самое если вы соедените вход светодиода с 5 вольт (+5v или VCC)

delay(1000);                       //задерживает код на время 1000 милисекунд = 1 секунда [очень просто использовать, далее откажемся от неё]
               
}
```
Вдумайтесь в код, и проанализирутей логику, поменяйте значения задержки в delay()

```
Задача для закрепления - мигать светодиодом с 
периодом: 0.5 секунды 
длительностью горения: 0.2 секунды
```

Теперь подключите 2 светодиод к другому любому порту платы, все аналогично.
2 Задача:

мигать 1 светодиодом с 
периодом: 1 секунды 
длительностью горения: 1 секунды

и ОДНОВРЕМННО

мигать 2 светодиодом с 
периодом: 0.2 секунды 
длительностью горения: 0.1 секунды


***
Можно ли реализвать это с использованием delay, без использования переменных и функций?
- Нет, одна бОльшая заддержка (1 сек для 1 свет-да) будет мешать дргуой, точнее блокировать другую, в итоге мы получим полный разнобой. Delay() - работает синхронно. 

Решение есть, смотрим на этот алгогритм, более подробно в видео https://youtu.be/hYDp4dcv4Uc:
![alt text](https://alexgyver.ru/wp-content/uploads/2021/07/howto.jpg)

1. Мы не привязывем мигание к реальному времени (например включить в 13:00:00, а выключить в 13:00:01)
2. Задач несколько -  мигать 2мя свет-ми
3. Тайминги не жесткие (например 1 микросекунда)
4. Самое главное не использоать delay, введь мы учимся писать ХОРОШИЙ КОД, значит будем использовать логику на millis()
![alt text](/uploads/14554b120e866ce4e7b0a287f009656a/image.png)

```c++
#define MY_PERIOD 500  // период в мс
uint32_t tmr1;         // переменная таймера
void setup() {}
void loop() {
  if (millis() - tmr1 >= MY_PERIOD) {  // ищем разницу
    tmr1 = millis();                   // сброс таймера
    // выполнить действие
  }
}
```

Решение задачи теперь выглядит так, в данном варианте период и длительность одинаковы и задаются переменной PERIOD_,
```c++
#define PERIOD_1 500   // период 1 в мс
#define PERIOD_2 1000  // период 2 в мс

#define LED_PIN_1 7
#define LED_PIN_2 8

uint32_t tmr1;         // ГЛОБАЛЬНАЯ переменная таймера 1
uint32_t tmr2;         // ГЛОБАЛЬНАЯ переменная таймера 2


void setup() {
    pinMode(LED_PIN_1, OUTPUT);
    pinMode(LED_PIN_2, OUTPUT);
}

void loop() {
  if (millis() - tmr1 >= PERIOD_1) {  // ищем разницу
    tmr1 = millis();                   // сброс таймера
    // выполнить действие
    digitalWrite(LED_PIN_1, !digitalRead(LED_PIN_1)); //Переключаем состояние порта для 1 свет-да
  }

  if (millis() - tmr2 >= PERIOD_2) {  // ищем разницу
    tmr2 = millis();                   // сброс таймера
    // выполнить действие
    digitalWrite(LED_PIN_2, !digitalRead(LED_PIN_2)); //Переключаем состояние порта для 2 свет-да
  }
}
```

```
Задача для закрепления:
Попробуйте добваить, опцию длительности горения светодида
```



Теперь усложним схему, добавим датчик DHT11 (или дргуой датчик):

схема

```
Задача для закрепления:
При изменении температруы на +-1 градус или влажности мигать 1 светодидом 0.1 сек, а также вывводить показания в Serial порт. Если произошел отказ датчика (не подключен) - мигать 2 светодидом с перидом и длительностью 0.5 сек.


Вам нужно объеденить/доработать куски кода в одну программу, ИСПОЛЬЗОВАТЬ delay() - не допускается.
```

Работа с DHT 11 / 22
https://arduinomaster.ru/datchiki-arduino/datchiki-temperatury-i-vlazhnosti-dht11-dht22/
https://github.com/adafruit/DHT-sensor-library

```c++
#include "DHT.h"
#define DHTPIN 2 // Пин датчика
// Одна из следующих строк закоментирована. Снимите комментарий, если подключаете датчик DHT11 к arduino
DHT dht(DHTPIN, DHT11); //Инициация датчика
//DHT dht(DHTPIN, DHT22); - для DHT22

void setup() {
  Serial.begin(9600);
  dht.begin();
}
void loop() {
  delay(2000); // 2 секунды задержки
  float h = dht.readHumidity(); //Измеряем влажность
  float t = dht.readTemperature(); //Измеряем температуру
  if (isnan(h) || isnan(t)) {  // Проверка. Если не удается считать показания, выводится «Ошибка считывания», и программа завершает работу
    Serial.println("Ошибка считывания");
    return;
  }
  Serial.print("Влажность: ");
  Serial.print(h);
  Serial.print(" %\t");
  Serial.print("Температура: ");
  Serial.print(t);
  Serial.println(" *C "); //Вывод показателей на экран
}
```

Serial порт
```c++

void setup() {
  Serial.begin(9600); // Скорость порта
}
void loop() {
  Serial.print("-> "); //печатает 2 символа 
  Serial.println("12"); //печатает 2 символа и добовлчет символ конца строки \n

  Serial.print("-> "); //печатает 2 символа 
  Serial.println("56"); //печатает 2 символа и добовлчет символ конца строки \n
  // Получим в мониторе порта
  //-> 12
  //-> 56
}
```

https://github.com/GyverLibs/GyverTimer


```c++
#include <Arduino.h>

#include "GyverTimer.h"


GTimer myTimer(MS);               // создать миллисекундный таймер

void setup() {
  Serial.begin(9600);
  myTimer.setInterval(500);   // настроить интервал
}


void loop() {
  if (myTimer.isReady()) Serial.println("Timer!");
}
```

```c++
void setInterval(uint32_t interval);	// установка интервала работы таймера (также запустит и сбросит таймер) - режим интервала
void setTimeout(uint32_t timeout);		// установка таймаута работы таймера (также запустит и сбросит таймер) - режим таймаута
boolean isReady();						// возвращает true, когда пришло время
boolean isEnabled();					// вернуть состояние таймера (остановлен/запущен)
void reset();							// сброс таймера на установленный период работы
void start();							// запустить/перезапустить (со сбросом счёта)
void stop();							// остановить таймер (без сброса счёта)	
void resume();							// продолжить (без сброса счёта)	

// служебное
void setMode(boolean mode);				// установка режима работы вручную: AUTO или MANUAL (TIMER_INTERVAL / TIMER_TIMEOUT)
```



#Решение


```c++
#include <Arduino.h>
#include "GyverTimer.h"
#include "DHT.h"

// TIMER_INTERVAL = 0 Timer_2.setMode(0);
// TIMER_TIMEOUT = 1 Timer_2.setMode(1);

#define DHTPIN 2 // Пин датчика
// Одна из следующих строк закоментирована. Снимите комментарий, если подключаете датчик DHT11 к arduino
DHT dht(DHTPIN, DHT11); // Инициация датчика
// DHT dht(DHTPIN, DHT22); - для DHT22

#define LED_PIN_BLINK 7
#define LED_PIN_ERROR 8

// MS - время в милисекундах !!!
// US - время в микросекундах  !!!

GTimer dht_timer(MS); // для опроса датчика
GTimer led_timer(MS); // для мигания свет-да

void setup()
{
  Serial.begin(9600);
  dht.begin();

  dht_timer.setInterval(2000); // интервал опроса датчика

  pinMode(LED_PIN_BLINK, OUTPUT);
  pinMode(LED_PIN_ERROR, OUTPUT);
}

void loop()
{
  static int dht_state = 1;

  if (dht_timer.isReady())
  {
    static float last_t, last_h;

    float h = dht.readHumidity();    // Измеряем влажность
    float t = dht.readTemperature(); // Измеряем температуру

    if (isnan(h) || isnan(t))
    { // Проверка. Если не удается считать показания, выводится «Ошибка считывания», и программа завершает работу
      Serial.println("Ошибка считывания");

      if (dht_state)
      {
        digitalWrite(LED_PIN_BLINK, LOW);
        led_timer.setInterval(1000);
        dht_state = 0;
      }
    }
    else
    {
      if (!dht_state)
      {
        digitalWrite(LED_PIN_ERROR, LOW);
        dht_state = 1;
      }

      if (fabs(t - last_t) >= 1 || fabs(h - last_h) >= 1)
      {

        last_h = h;
        last_t = t; // Присваивание тек. значения

        led_timer.setMode(TIMER_TIMEOUT); // режим таймер с запуском
        led_timer.setTimeout(200);        // после изменения темп/влаж гореть 200 мс.

        led_timer.start();
        digitalWrite(LED_PIN_BLINK, 1);

        Serial.print("Влажность: ");
        Serial.print(h);
        Serial.print(" %\t");
        Serial.print("Температура: ");
        Serial.print(t);
        Serial.println(" *C "); // Вывод показателей на экран
      }
    }
  }

  if (led_timer.isReady())
  {
    if (dht_state)
      digitalWrite(LED_PIN_BLINK, LOW);
    else
      digitalWrite(LED_PIN_ERROR, !digitalRead(LED_PIN_ERROR));
  }
}
```









Решение
```c++

```

```
добавим теперь в нашу схему реле и кнопку, доработаем логику:
Сделаем простую систему климат-контроля, работающая в двух режимах:
- автоматический: после повышения определенной температруы(заданой переменной) включить реле (например вентилятор), после снижения - выкл.
- ручной:  вкл/откл вентилятора не зависит от температруы: включать его можно будет кнопкой по нажатию кнопки (вкл/откл).

переключение режимов реализовать длительным нажатием кнопки
События о изменениии состояние реле/ режима работа - выводить в Serial порт.

```

Полезные и крутые библиотеки:
https://github.com/GyverLibs/EncButton

```c++
#include <EncButton.h>

#DEFINE BTN_PIN 7
EncButton<EB_TICK, BTN_PIN> enc;        // просто кнопка <KEY>

void setup() {
  Serial.begin(9600);
  //enc.setButtonLevel(HIGH);     // уровень кнопки: LOW - кнопка подключает GND (по умолч.), HIGH - кнопка подключает VCC
  //enc.setHoldTimeout(1000);     // установить время удержания кнопки, мс (до 8 000)


void loop() {
  enc.tick();                     // опрос происходит здесь
  if (enc.press()) Serial.println("Press");
  if (enc.hold()) Serial.println("Hold");
}
```

